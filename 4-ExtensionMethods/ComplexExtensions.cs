﻿using System;

namespace ExtensionMethods {

    public static class ComplexExtensions
    {
        public static IComplex Add(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real + c2.Real, c1.Imaginary + c2.Imaginary);
        }

        public static IComplex Subtract(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real - c2.Real, c1.Imaginary - c2.Imaginary);
        }

        public static IComplex Multiply(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real * c2.Real - c1.Imaginary * c2.Imaginary, c1.Real * c2.Imaginary + c2.Real * c1.Imaginary);
        }

        public static IComplex Divide(this IComplex c1, IComplex c2)
        {
            double power = Math.Pow(c2.Modulus, 2);
            Console.WriteLine("{0},{1}", (c1.Real * c2.Real + c1.Imaginary * c2.Imaginary) / power, (c2.Real * c1.Imaginary - c1.Real * c2.Imaginary) / power);
            return new Complex((c1.Real * c2.Real + c1.Imaginary * c2.Imaginary) / power, (c2.Real * c1.Imaginary - c1.Real * c2.Imaginary) / power);
            //IComplex c = new Complex(c1.Real * c2.Real + c1.Imaginary * c2.Imaginary, c2.Real * c1.Imaginary - c1.Real * c2.Imaginary);
            //Console.WriteLine("{0},{1}", c.Real / power, c.Imaginary / power);
            //return new Complex(c.Real / power, c.Imaginary / power);
        }

        public static IComplex Conjugate(this IComplex c1)
        {
            return new Complex(c1.Real, -c1.Imaginary);
        }

        public static IComplex Reciprocal(this IComplex c1)
        {
            double power = Math.Pow(c1.Modulus,2);
            return new Complex(c1.Real / power, c1.Imaginary / power);
        }
    }

}