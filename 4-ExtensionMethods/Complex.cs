﻿using System;

namespace ExtensionMethods
{
    internal class Complex : IComplex
    {
        private double re;
        private double im;

        public Complex(double re, double im)
        {
            this.re = re;
            this.im = im;
        }

        public bool Equals(IComplex other)
        {
            return (this.re == other.Real && this.im == other.Imaginary);
        }

        public double Real
        {
            get
            {
                return re;
            }
        }

        public double Imaginary
        {
            get
            {
                return im;
            }
        }
        public double Modulus
        {
            
            get
            {
                double pow = 2;
                double power = Math.Pow(re, pow) + Math.Pow(im, pow);
                return Math.Sqrt(power);
            }
        }
        public double Phase
        {
            get
            {
                return Math.PI;
            }
        }

        public override string ToString()
        {
            // TODO improve
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            // TODO improve
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            // TODO improve
            return base.GetHashCode();
        }

    }
}